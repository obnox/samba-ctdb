                   ==============================
                   Release Notes for Samba 3.2.11
			   April 17, 2009
                   ==============================


This is a maintenance release of the Samba 3.2 series.

Major enhancements in 3.2.11 include:

    o Fix domain logins for WinXP clients pre SP3 (bug #6263).
    o Fix samr_OpenDomain access checks (bug #6089).
    o Fix smbd crash for close_on_completion.


######################################################################
Changes
#######

Changes since 3.2.10
--------------------


o   Jeremy Allison <jra@samba.org>
    * BUG 6089: Fix samr_OpenDomain access checks.
    * BUG 6254: Fix IPv6 PUT/GET errors to an SMB server (3.3) with
      "msdfs root" set to "yes".
    * Allow pdbedit to change a user rid/sid.
    * When doing a cli_ulogoff don't invalidate the cnum, invalidate the vuid.


o   Günther Deschner <gd@samba.org>
    * BUG 6205: Correct sample smb.conf share configuration.
    * BUG 6263: Fix domain logins for WinXP clients pre SP3.
    * Fix resume command typo for "printing = vlp".


o   Volker Lendecke <vl@samba.org>
    * Fix smbd crash for close_on_completion.
    * Fix a memleak in an unlikely error path in change_notify_create().


o   Jim McDonough <jmcd@samba.org>
    * Don't look up local user for remote changes, even when root.


######################################################################
Reporting bugs & Development Discussion
#######################################

Please discuss this release on the samba-technical mailing list or by
joining the #samba-technical IRC channel on irc.freenode.net.

If you do report problems then please try to send high quality
feedback. If you don't provide vital information to help us track down
the problem then you will probably be ignored.  All bug reports should
be filed under the Samba 3.2 product in the project's Bugzilla
database (https://bugzilla.samba.org/).


======================================================================
== Our Code, Our Bugs, Our Responsibility.
== The Samba Team
======================================================================

